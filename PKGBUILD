# Maintainer:  Håvard Pettersson <mail@haavard.me>
# Contributor: Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Thorsten Töpper <atsutane-tu@freethoughts.de>
# Contributor: Thayer Williams <thayer@archlinux.org>
# Contributor: Jeff 'codemac' Mickey <jeff@archlinux.org>

_pkgname=dmenu
pkgver=5.0
pkgname=${_pkgname}
pkgrel=1
pkgdesc="A generic menu for X"
url="http://tools.suckless.org/dmenu/"
arch=('i686' 'x86_64')
license=('MIT')
depends=('sh' 'libxinerama' 'libxft')
makedepends=('git')
provides=($_pkgname)
conflicts=($_pkgname)
source=(https://dl.suckless.org/tools/$pkgname-${pkgver}.tar.gz
	my-patch.diff
	my-fixes.diff)
sha256sums=('fe18e142c4dbcf71ba5757dbbdea93b1c67d58fc206fc116664f4336deef6ed3'
            'c6d4c3901a84895d7c5d43414251761f144dc985669dd1340b3db9a82c103402'
            'b275bda880044f9623dd1e65b6429a2eadf0420c035ba67d4a0505b8bc55bb1d')

prepare() {
  patch --directory="${srcdir}/${pkgname}-${pkgver}" --strip=1 < my-patch.diff
  cp ${srcdir}/${pkgname}-${pkgver}/config.def.h $BUILDDIR/config.def.h
  cp ${srcdir}/${pkgname}-${pkgver}/config.def.h $BUILDDIR/config.h
  patch --directory="$BUILDDIR" --strip=0 < $BUILDDIR/my-fixes.diff
}

build(){
  cd "${srcdir}/${pkgname}-${pkgver}"
  cp "$BUILDDIR/config.h" config.h
  make \
    X11INC=/usr/include/X11 \
    X11LIB=/usr/lib/X11
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make PREFIX=/usr DESTDIR="$pkgdir" install
  install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}
